package com.leonardoalves.uoleducacao.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonardoalves.data.entity.Shot;
import com.leonardoalves.uoleducacao.R;
import com.leonardoalves.uoleducacao.presenter.ShotDetail.ShotDetailPresenter;
import com.leonardoalves.uoleducacao.presenter.ShotDetail.ShotDetailPresenterListener;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leona on 24/09/2017.
 */

public class ShotDetailActivity extends AppCompatActivity {
    @BindView(R.id.shot_detail_title)
    TextView shotTitle;
    @BindView(R.id.shot_author)
    TextView shotAuthor;
    @BindView(R.id.shot_description)
    TextView shotDescription;
    @BindView(R.id.shot_image)
    ImageView shotImage;
    @BindView(R.id.view_count)
    TextView viewsCount;
    @BindView(R.id.comment_count)
    TextView commentsCount;
    @BindView(R.id.like_count)
    TextView likesCount;

    ShotDetailPresenter presenter = new ShotDetailPresenter();
    ShotDetailPresenterListener presenterListener = new PresenterListener();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_detail);
        ButterKnife.bind(this);
        String value = getIntent().getExtras().getString("id");
        presenter.getDetailData(Integer.valueOf(value), presenterListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class PresenterListener implements ShotDetailPresenterListener{
        @Override
        public void showDetail(Shot shot) {
            shotTitle.setText(shot.getTitle());
            shotAuthor.setText(shot.getUser().getName());
            shotDescription.setText(shot.getDescription());
            Picasso.with(ShotDetailActivity.this).load(shot.getImages().getNormal()).into(shotImage);
            viewsCount.setText(shot.getViewsCount().toString());
            viewsCount.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                 @Override
                 public void onGlobalLayout() {
                     Drawable img = ContextCompat.getDrawable(ShotDetailActivity.this, R.drawable.ic_view);
                     img.setBounds(0, 0, img.getIntrinsicWidth() * viewsCount.getMeasuredHeight() / img.getIntrinsicHeight(), viewsCount.getMeasuredHeight());
                     viewsCount.setCompoundDrawables(img, null, null, null);
                     viewsCount.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                 }
            });
            viewsCount.setText(shot.getViewsCount().toString());
            viewsCount.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Drawable img = ContextCompat.getDrawable(ShotDetailActivity.this, R.drawable.ic_view);
                    img.setBounds(0, 0, img.getIntrinsicWidth() * viewsCount.getMeasuredHeight() / img.getIntrinsicHeight(), viewsCount.getMeasuredHeight());
                    viewsCount.setCompoundDrawables(img, null, null, null);
                    viewsCount.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
            commentsCount.setText(shot.getCommentsCount().toString());
            commentsCount.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Drawable img = ContextCompat.getDrawable(ShotDetailActivity.this, R.drawable.ic_comment);
                    img.setBounds(0, 0, img.getIntrinsicWidth() * commentsCount.getMeasuredHeight() / img.getIntrinsicHeight(), commentsCount.getMeasuredHeight());
                    commentsCount.setCompoundDrawables(img, null, null, null);
                    commentsCount.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
            likesCount.setText(shot.getLikesCount().toString());
            likesCount.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Drawable img = ContextCompat.getDrawable(ShotDetailActivity.this, R.drawable.ic_love);
                    img.setBounds(0, 0, img.getIntrinsicWidth() * likesCount.getMeasuredHeight() / img.getIntrinsicHeight(), likesCount.getMeasuredHeight());
                    likesCount.setCompoundDrawables(img, null, null, null);
                    likesCount.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });


        }

        @Override
        public void showError(String message) {

        }
    }
}
