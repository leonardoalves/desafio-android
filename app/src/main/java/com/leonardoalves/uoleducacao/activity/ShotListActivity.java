package com.leonardoalves.uoleducacao.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.leonardoalves.data.constrains.Constrains;
import com.leonardoalves.data.enums.SortOptions;
import com.leonardoalves.uoleducacao.R;
import com.leonardoalves.uoleducacao.adapter.ShotListItemAdapter;
import com.leonardoalves.uoleducacao.presenter.ShotLists.GetShotsListener;
import com.leonardoalves.uoleducacao.presenter.ShotLists.ShotListPresenter;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class ShotListActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;
    private ShotListPresenter presenter;
    private GridLayoutManager layoutManager;
    private ShotListItemAdapter adapter;

    private boolean isLoading = true;
    private boolean isLastPage = false;

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new ScrollListener();
    private PresenterListeners  presenterListeners =  new PresenterListeners();
    private SwipeToRefreshData swipeToRefreshDataListener = new SwipeToRefreshData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_list);
        ButterKnife.bind(this);
        presenter = new ShotListPresenter();
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ShotListItemAdapter(presenter);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        recyclerView.setItemAnimator(new SlideInUpAnimator());
        swipeRefreshLayout.setRefreshing(true);
        presenter.loadNextPage(presenterListeners);
        swipeRefreshLayout.setOnRefreshListener(swipeToRefreshDataListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sort_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.popularity:
                adapter.notifyItemRangeRemoved(0, presenter.getDatasetSize());
                isLoading = true;
                swipeRefreshLayout.setRefreshing(true);
                presenter.changeSort(SortOptions.popularity, presenterListeners);
                return true;
            case R.id.comments:
                adapter.notifyItemRangeRemoved(0, presenter.getDatasetSize());
                isLoading = true;
                swipeRefreshLayout.setRefreshing(true);
                presenter.changeSort(SortOptions.comments, presenterListeners);
                return true;
            case R.id.recent:
                adapter.notifyItemRangeRemoved(0, presenter.getDatasetSize());
                isLoading = true;
                swipeRefreshLayout.setRefreshing(true);
                presenter.changeSort(SortOptions.recent, presenterListeners);
                return true;
            case R.id.views:
                adapter.notifyItemRangeRemoved(0, presenter.getDatasetSize());
                isLoading = true;
                swipeRefreshLayout.setRefreshing(true);
                presenter.changeSort(SortOptions.views, presenterListeners);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class PresenterListeners implements GetShotsListener{
        @Override
        public void shotsPage(int numberInsertions) {
            if (numberInsertions < Constrains.PAGE_SIZE){
                isLastPage = true;
            }
            adapter.notifyItemRangeInserted(presenter.getDatasetSize(), numberInsertions);
            isLoading = false;
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private class ScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= Constrains.PAGE_SIZE) {
                    isLoading = true;
                    swipeRefreshLayout.setRefreshing(true);
                    presenter.loadNextPage(presenterListeners);
                }
            }
        }
    };

    private class SwipeToRefreshData implements  SwipeRefreshLayout.OnRefreshListener{

        @Override
        public void onRefresh() {
            adapter.notifyItemRangeRemoved(0, presenter.getDatasetSize());
            presenter.refresh(presenterListeners);
        }
    }
}
