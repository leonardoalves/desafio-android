package com.leonardoalves.uoleducacao.adapter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonardoalves.data.viewEntity.ShotsShortList;
import com.leonardoalves.uoleducacao.R;
import com.leonardoalves.uoleducacao.activity.ShotDetailActivity;
import com.leonardoalves.uoleducacao.presenter.ShotLists.ShotListPresenter;
import com.squareup.picasso.Picasso;

/**
 * Created by leona on 24/09/2017.
 */

public class ShotListItemAdapter extends RecyclerView.Adapter<ShotListItemAdapter.ViewHolder> {

    private ShotListPresenter presenter;

    public ShotListItemAdapter(ShotListPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.shot_short_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflatedView);
        return viewHolder;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ShotsShortList shotViewData = presenter.getShotViewData(position);
        holder.title.setText(shotViewData.getTitle());
        holder.viewsCount.setText(shotViewData.getViewsCount().toString());
        holder.viewsCount.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Drawable img = ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_view);
                        img.setBounds(0, 0, img.getIntrinsicWidth() * holder.viewsCount.getMeasuredHeight() / img.getIntrinsicHeight(), holder.viewsCount.getMeasuredHeight());
                        holder.viewsCount.setCompoundDrawables(img, null, null, null);
                        holder.viewsCount.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
        holder.attachmentsCount.setText(shotViewData.getAttachmentsCount().toString());
        holder.attachmentsCount.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Drawable img = ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_reply);
                        img.setBounds(0, 0, img.getIntrinsicWidth() * holder.attachmentsCount.getMeasuredHeight() / img.getIntrinsicHeight(), holder.attachmentsCount.getMeasuredHeight());
                        holder.attachmentsCount.setCompoundDrawables(img, null, null, null);
                        holder.attachmentsCount.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
        Picasso.with(holder.itemView.getContext()).load(shotViewData.getImageUrl()).into(holder.thumb);
    }

    @Override
    public int getItemCount() {
        return presenter.getDatasetSize();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        ImageView thumb;
        TextView viewsCount;
        TextView commentsCount;
        TextView attachmentsCount;
        TextView userName;

        public ViewHolder(final View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.shot_title);
            viewsCount = (TextView) itemView.findViewById(R.id.view_count);
            attachmentsCount = (TextView) itemView.findViewById(R.id.reply_count);
            thumb = (ImageView) itemView.findViewById(R.id.thumb);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int adapterPosition = getAdapterPosition();
                    Intent intent = new Intent(v.getContext(), ShotDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", presenter.getShotId(adapterPosition).toString());
                    intent.putExtras(bundle);
                    v.getContext().startActivity(intent);
                }
            });
        }
    }
}
