package com.leonardoalves.uoleducacao.presenter.ShotDetail;

import com.leonardoalves.data.entity.Shot;
import com.leonardoalves.domain.GetShot;
import com.leonardoalves.domain.GetShotDetailListener;

/**
 * Created by leona on 24/09/2017.
 */

public class ShotDetailPresenter {
    public void getDetailData(int id, final ShotDetailPresenterListener listener){
        GetShot getShot = new GetShot();
        getShot.getShotDetail(id, new GetShotDetailListener() {
            @Override
            public void onResult(Shot shot) {
                listener.showDetail(shot);
            }

            @Override
            public void onError(String message) {
                listener.showError(message);
            }
        });
    }
}
