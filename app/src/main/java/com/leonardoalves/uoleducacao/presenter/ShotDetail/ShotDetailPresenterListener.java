package com.leonardoalves.uoleducacao.presenter.ShotDetail;

import com.leonardoalves.data.entity.Shot;

/**
 * Created by leona on 25/09/2017.
 */

public interface ShotDetailPresenterListener {
    void showDetail(Shot shot);
    void showError(String message);
}
