package com.leonardoalves.uoleducacao.presenter.ShotLists;

import com.leonardoalves.data.viewEntity.ShotsShortList;

import java.util.ArrayList;

/**
 * Created by leona on 24/09/2017.
 */

public interface GetShotsListener {
    void shotsPage(int numberInsertions);
}
