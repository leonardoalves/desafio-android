package com.leonardoalves.uoleducacao.presenter.ShotLists;

import com.leonardoalves.data.entity.Shot;
import com.leonardoalves.data.enums.SortOptions;
import com.leonardoalves.data.viewEntity.ShotsShortList;
import com.leonardoalves.domain.GetShot;
import com.leonardoalves.domain.GetShotListPageListener;

import java.util.ArrayList;

/**
 * Created by leona on 24/09/2017.
 */

public class ShotListPresenter {
    ArrayList<Shot> dataset= new ArrayList<>();
    long page = 0;
    SortOptions sortBy = SortOptions.popularity;

    private void getShots(long page, SortOptions sortBy, final GetShotsListener listener){
        GetShot getShot = new GetShot();
        getShot.getShotListPage(page, sortBy, new GetShotListPageListener() {
            @Override
            public void onResult(ArrayList<Shot> shots) {
                dataset.addAll(shots);
                listener.shotsPage(shots.size());
            }

            @Override
            public void onError(String message) {
                System.out.println(message);
            }
        });
    }

    public ShotsShortList getShotViewData(int position){
        return new ShotsShortList(dataset.get(position));
    }

    public int getDatasetSize(){
        return dataset.size();
    }

    public void loadNextPage(final GetShotsListener listener) {
        getShots(page, sortBy, listener);
        page++;
    }

    public void refresh(final GetShotsListener listener) {
        dataset.clear();
        page = 0;
        loadNextPage(listener);
    }

    public Integer getShotId(int position){
        return dataset.get(position).getId();
    }

    public void changeSort(SortOptions sortBy, final GetShotsListener listener){
        this.sortBy = sortBy;
        this.refresh(listener);
    }
}
