package com.leonardoalves.data.enums;

/**
 * Created by leona on 25/09/2017.
 */

public enum SortOptions {
    popularity("popularity"),
    comments("comments"),
    recent("recent"),
    views("views");

    private final String name;

    SortOptions(String name) {
        this.name = name;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
