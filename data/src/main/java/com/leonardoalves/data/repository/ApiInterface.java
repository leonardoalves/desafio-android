package com.leonardoalves.data.repository;

import com.leonardoalves.data.entity.Shot;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by leona on 24/09/2017.
 */

public interface ApiInterface {
    @GET("shots/")
    Call<ArrayList<Shot>> getShots(@Query("page") long page, @Query("per_page") int perPage);
    @GET("shots")
    Call<ArrayList<Shot>> getShotsSorted(@Query("page") long page, @Query("per_page") int perPage, @Query("sort") String sort);
    @GET("shots/{id}/")
    Call<Shot> getShot(@Path("id") int id);
}
