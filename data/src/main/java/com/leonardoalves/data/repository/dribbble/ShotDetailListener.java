package com.leonardoalves.data.repository.dribbble;

import com.leonardoalves.data.entity.Shot;

/**
 * Created by leona on 24/09/2017.
 */

public interface ShotDetailListener {
    void onResult(Shot shot);
    void onError(Throwable t);
}
