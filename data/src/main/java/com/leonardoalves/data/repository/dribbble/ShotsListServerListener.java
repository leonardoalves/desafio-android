package com.leonardoalves.data.repository.dribbble;

import com.leonardoalves.data.entity.Shot;

import java.util.ArrayList;

/**
 * Created by leona on 24/09/2017.
 */

public interface ShotsListServerListener {

    void onResult(ArrayList<Shot> sandwiches);

    void onError(Throwable t);
}
