package com.leonardoalves.data.repository.dribbble;

import android.util.Log;

import com.leonardoalves.data.constrains.Constrains;
import com.leonardoalves.data.entity.Shot;
import com.leonardoalves.data.repository.ApiClient;
import com.leonardoalves.data.repository.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leona on 24/09/2017.
 */

public class ShotsServer {
    private static final String TAG = "GET_SHOT_LIST";

    public static void getShotsList(long page, final ShotsListServerListener listener){

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<Shot>> call = apiService.getShots(page, Constrains.PAGE_SIZE);
        call.enqueue(new Callback<ArrayList<Shot>>() {
            @Override
            public void onResponse(Call<ArrayList<Shot>>call, Response<ArrayList<Shot>> response) {
                ArrayList<Shot> shots = response.body();
                listener.onResult(shots);
            }

            @Override
            public void onFailure(Call<ArrayList<Shot>>call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onError(t);
            }
        });
    }

    public static void getSortedShotsList(long page, String sortBy, final ShotsListServerListener listener){

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<Shot>> call = apiService.getShotsSorted(page, Constrains.PAGE_SIZE, sortBy);
        call.enqueue(new Callback<ArrayList<Shot>>() {
            @Override
            public void onResponse(Call<ArrayList<Shot>>call, Response<ArrayList<Shot>> response) {
                ArrayList<Shot> shots = response.body();
                listener.onResult(shots);
            }

            @Override
            public void onFailure(Call<ArrayList<Shot>>call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onError(t);
            }
        });
    }

    public static void getShotDetail(int id, final ShotDetailListener listener){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Shot> call = apiService.getShot(id);
        call.enqueue(new Callback<Shot>() {
            @Override
            public void onResponse(Call<Shot>call, Response<Shot> response) {
                Shot shot = response.body();
                listener.onResult(shot);
            }

            @Override
            public void onFailure(Call<Shot>call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onError(t);
            }
        });
    }
}
