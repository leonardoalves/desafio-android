package com.leonardoalves.data.viewEntity;

import com.leonardoalves.data.entity.Shot;

/**
 * Created by leona on 24/09/2017.
 */

public class ShotsShortList {
    private String title;
    private Integer viewsCount;
    private Integer commentsCount;
    private Integer attachmentsCount;
    private String userName;
    private String imageUrl;

    public ShotsShortList(String title, Integer viewsCount, Integer commentsCount, Integer attachmentsCount, String userName, String imageUrl) {
        this.title = title;
        this.viewsCount = viewsCount;
        this.commentsCount = commentsCount;
        this.attachmentsCount = attachmentsCount;
        this.userName = userName;
        this.imageUrl = imageUrl;
    }

    public ShotsShortList(Shot shot){
        this.title = shot.getTitle();
        this.viewsCount = shot.getViewsCount();
        this.commentsCount = shot.getViewsCount();
        this.attachmentsCount = shot.getAttachmentsCount();
        this.userName = shot.getUser().getUsername();
        this.imageUrl = shot.getImages().getTeaser();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(Integer viewsCount) {
        this.viewsCount = viewsCount;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Integer getAttachmentsCount() {
        return attachmentsCount;
    }

    public void setAttachmentsCount(Integer attachmentsCount) {
        this.attachmentsCount = attachmentsCount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
