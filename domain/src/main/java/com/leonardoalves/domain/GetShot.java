package com.leonardoalves.domain;

import com.leonardoalves.data.entity.Shot;
import com.leonardoalves.data.enums.SortOptions;
import com.leonardoalves.data.repository.dribbble.ShotDetailListener;
import com.leonardoalves.data.repository.dribbble.ShotsListServerListener;
import com.leonardoalves.data.repository.dribbble.ShotsServer;

import java.util.ArrayList;

/**
 * Created by leona on 24/09/2017.
 */

public class GetShot {
    public void getShotListPage(long page, SortOptions sortBy, final GetShotListPageListener listener){
        if (sortBy == SortOptions.popularity) {
            ShotsServer.getShotsList(page, new ShotsListServerListener() {
                @Override
                public void onResult(ArrayList<Shot> sandwiches) {
                    listener.onResult(sandwiches);
                }

                @Override
                public void onError(Throwable t) {
                    listener.onError(t.getMessage());
                }
            });
            return;
        }

        ShotsServer.getSortedShotsList(page, sortBy.toString(), new ShotsListServerListener() {
            @Override
            public void onResult(ArrayList<Shot> sandwiches) {
                listener.onResult(sandwiches);
            }

            @Override
            public void onError(Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }

    public void getShotDetail(int id, final GetShotDetailListener listener){
        ShotsServer.getShotDetail(id, new ShotDetailListener() {
            @Override
            public void onResult(Shot shot) {
                listener.onResult(shot);
            }

            @Override
            public void onError(Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }
}
