package com.leonardoalves.domain;

import com.leonardoalves.data.entity.Shot;

/**
 * Created by leona on 24/09/2017.
 */

public interface GetShotDetailListener {
    void onResult(Shot shot);
    void onError(String message);
}
