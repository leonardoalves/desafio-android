package com.leonardoalves.domain;

import com.leonardoalves.data.entity.Shot;

import java.util.ArrayList;

/**
 * Created by leona on 24/09/2017.
 */

public interface GetShotListPageListener {
    void onResult(ArrayList<Shot> sandwiches);
    void onError(String message);

}
